# Machine Learning

This repository contains the implementations of different machine learning algorithms for data analysis using R and Python.

## Info

It will be updated regularly so the content might change over time.

## View in Notebook Format

Choose the notebook you want to view and click on open raw. Copy the link from url.
Past the copied url in the text box at https://nbviewer.jupyter.org/


Eg:
	Pickle - https://nbviewer.jupyter.org/urls/bitbucket.org/tekrajchhetri/machinelearning/raw/c52232867280be3a143ac7e8471bf2d0b82641fb/Data%20Acquisition%20Python/Data%20Acquisition%20%20--%20Pandas%20_%20Pickles.ipynb 


